package grupo2;

public class Funcionario {
    /**
     * Nome Funcionário
     */
    private String nome;

    /**
     * Salário do Funcionário
     */
    private Double salario;


    /**
     * Construtor com parâmetros
     * @param _nome
     * @param _salario
     */
    public Funcionario(String _nome, Double _salario) {
        this.nome = _nome;
        this.salario = _salario;
    }

    /**
     *
     * @return nome
     */
    public String getNome() {
        return nome;
    }

    /**
     *
     * @param _nome
     */
    public void setNome(String _nome) {
        this.nome = _nome;
    }

    /**
     *
     * @return salário
     */
    public Double getSalario() {
        return salario;
    }

    /**
     *
     * @param _salario
     */
    public void setSalario(Double _salario) {
        this.salario = _salario;
    }
}


